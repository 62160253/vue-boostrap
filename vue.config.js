module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160253/learn_bootstrap/'
    : '/'
}
